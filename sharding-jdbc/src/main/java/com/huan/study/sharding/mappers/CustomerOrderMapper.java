package com.huan.study.sharding.mappers;

import com.huan.study.sharding.entity.CustomerOrder;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author huan.fu 2021/5/24 - 下午5:29
 */
public interface CustomerOrderMapper {

    int createOrder(@Param("orderId") BigDecimal orderId, @Param("customerId") Long customerId, @Param("sallerId") Long sallerId, @Param("productName") String productName);

    CustomerOrder findOrder(@Param("orderId") BigDecimal orderId);

    List<CustomerOrder> findCustomerOrders(@Param("customerId") Long customerId);
}
