package com.huan.study.sharding.entity;

import java.math.BigDecimal;

/**
 * @author huan.fu 2021/5/25 - 下午4:54
 */
public class CustomerOrderItem {
    private Long id;
    private BigDecimal orderId;
    private Long customerId;
    private String detail;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getOrderId() {
        return orderId;
    }

    public void setOrderId(BigDecimal orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
