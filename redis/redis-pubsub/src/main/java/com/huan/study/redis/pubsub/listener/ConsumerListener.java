package com.huan.study.redis.pubsub.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

import java.nio.charset.StandardCharsets;

/**
 * @author huan.fu 2021/11/12 - 下午2:10
 */
@Slf4j
public class ConsumerListener implements MessageListener {
    
    private final String consumerName;
    
    public ConsumerListener(String consumerName) {
        this.consumerName = consumerName;
    }
    
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String topicPattern = new String(pattern, StandardCharsets.UTF_8);
        String channel = new String(message.getChannel(), StandardCharsets.UTF_8);
        String body = new String(message.getBody(), StandardCharsets.UTF_8);
        log.info("消费者:[{}] 接收到消息:topicPattern:[{}],channel:[{}],body:[{}]", consumerName, topicPattern, channel, body);
    }
}
