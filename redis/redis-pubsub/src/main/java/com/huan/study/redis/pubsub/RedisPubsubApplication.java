package com.huan.study.redis.pubsub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan.fu 2021/11/12 - 下午2:07
 */
@SpringBootApplication
public class RedisPubsubApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(RedisPubsubApplication.class);
    }
}
