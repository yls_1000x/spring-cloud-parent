package com.huan.study.redis.pubsub.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 往 redis channel 中发布消息
 *
 * @author huan.fu 2021/11/12 - 下午4:58
 */
@RestController
@Slf4j
public class ProducerController {
    
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    
    /**
     * 向redis channel中发布消息
     */
    @GetMapping("publish/message")
    public String publishMessage(@RequestParam("message") String message, @RequestParam("channel") String channel) {
        stringRedisTemplate.convertAndSend(channel, message);
        return "ok";
    }
}
