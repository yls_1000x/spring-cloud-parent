package com.huan.cola.customer.dto.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author huan.fu
 * @date 2022/6/7 - 10:34
 */
@AllArgsConstructor
@Getter
public class CustomerCreateEvent {
    private Integer customerId;
}
