package com.huan.cola.customer.dto.qry;

import com.alibaba.cola.dto.Query;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class GetCustomerByPhoneQry extends Query {
    @NotBlank(message = "手机号不可为空")
    private String phone;
}
