package com.huan.cola.customer.dto.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {

    CUSTOMER_PHONE_EXISTS("100001", "客户手机号已经存在"),
    CUSTOMER_IS_VIP_FOR_ADD("100002", "vip客户不允许添加");

    private final String errCode;
    private final String errDesc;
}
