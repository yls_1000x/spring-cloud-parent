package com.huan.cola.customer.api;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.SingleResponse;
import com.huan.cola.customer.dto.cmd.CustomerAddCmd;
import com.huan.cola.customer.dto.cmd.CustomerUpdateCmd;
import com.huan.cola.customer.dto.data.CustomerDTO;
import com.huan.cola.customer.dto.qry.GetCustomerByPhoneQry;

public interface CustomerService {
    /**
     * 查找所有的客户
     */
    MultiResponse<CustomerDTO> findAll();

    /**
     * 根据手机号查询单个客户
     */
    SingleResponse<CustomerDTO> getCustomer(GetCustomerByPhoneQry getCustomerByPhoneQry);

    /**
     * 添加客户
     */
    SingleResponse<Boolean> addCustomer(CustomerAddCmd customerAddCmd);

    /**
     * 更新客户
     */
    SingleResponse<Boolean> updateCustomer(CustomerUpdateCmd customerUpdateCmd);

    /**
     * 根据手机号删除客户
     */
    SingleResponse<Boolean> deleteCustomer(String phone);
}
