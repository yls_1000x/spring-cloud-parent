package com.huan.cola.customer.dto.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CustomerDTO {
    private Integer id;
    private String phone;
    private String idCard;
    private String address;
    private Integer customerType;
    private String customerTypeDesc;
}
