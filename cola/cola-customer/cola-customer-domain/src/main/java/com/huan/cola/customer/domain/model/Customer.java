package com.huan.cola.customer.domain.model;

import com.huan.cola.customer.domain.model.vauleobject.CustomerType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Objects;

/**
 * @author huan.fu
 * @date 2022/6/7 - 10:42
 */
@Getter
@Setter
@ToString
public class Customer {
    private Integer id;
    private String phone;
    private String address;
    private String idCard;
    private CustomerType customerType;

    /**
     * 是否是vip客户
     */
    public boolean isVip() {
        return Objects.equals(this.customerType, CustomerType.VIP);
    }
}
