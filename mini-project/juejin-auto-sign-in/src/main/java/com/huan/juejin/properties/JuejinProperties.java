package com.huan.juejin.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author huan.fu 2021/11/18 - 下午5:19
 */
@ConfigurationProperties(prefix = "juejin")
@EnableConfigurationProperties({JuejinProperties.class})
@Component
public class JuejinProperties {
    
    private String cookie;
    
    public String getCookie() {
        return cookie;
    }
    
    public void setCookie(String cookie) {
        this.cookie = cookie;
    }
}
