package com.huan.flink.partition;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 全局分区也是一种特殊的分区方式。这种做法非常极端，通过调用.global()方法，会将所有的输入流数据都发送到下游算子的第一个并行子任务中去。这就相当于强行让下游任务并行度变成了1，所以使用这个操作需要非常谨慎，可能对程序造成很大的压力。
 *
 * @author huan.fu
 * @date 2023/9/23 - 06:51
 */
public class GlobalPartitionApplication {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        // 设置并行度为2
        environment.setParallelism(2);

        environment.fromElements(
                        new Student(1L, 1L, "张三"),
                        new Student(2L, 1L, "张三"),
                        new Student(3L, 2L, "张三"),
                        new Student(4L, 2L, "张三"),
                        new Student(5L, 2L, "张三"),
                        new Student(6L, 3L, "张三")
                )
                // 全局分区也是一种特殊的分区方式。这种做法非常极端，通过调用.global()方法，会将所有的输入流数据都发送到下游算子的第一个并行子任务中去。这就相当于强行让下游任务并行度变成了1，所以使用这个操作需要非常谨慎，可能对程序造成很大的压力。
                .global()
                .print();

        environment.execute("global job");

    }
}
