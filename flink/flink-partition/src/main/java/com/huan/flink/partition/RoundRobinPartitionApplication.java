package com.huan.flink.partition;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 轮询，简单来说就是“发牌”，按照先后顺序将数据做依次分发。通过调用DataStream的.rebalance()方法，就可以实现轮询重分区。rebalance使用的是Round-Robin负载均衡算法，可以将输入流数据平均分配到下游的并行任务中去。
 *
 * @author huan.fu
 * @date 2023/9/23 - 06:51
 */
public class RoundRobinPartitionApplication {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        // 设置并行度为2
        environment.setParallelism(2);

        environment.fromElements(
                        new Student(1L, 1L, "张三"),
                        new Student(2L, 1L, "张三"),
                        new Student(3L, 2L, "张三"),
                        new Student(4L, 2L, "张三"),
                        new Student(5L, 2L, "张三"),
                        new Student(6L, 3L, "张三")
                )
                // 轮询，简单来说就是“发牌”，按照先后顺序将数据做依次分发。通过调用DataStream的.rebalance()方法，就可以实现轮询重分区。rebalance使用的是Round-Robin负载均衡算法，可以将输入流数据平均分配到下游的并行任务中去。
                .rebalance()
                .print();

        environment.execute("round robin job");

    }
}
