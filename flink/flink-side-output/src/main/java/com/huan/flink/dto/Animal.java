package com.huan.flink.dto;

/**
 * 动物
 *
 * @author huan.fu
 * @date 2023/9/23 - 09:56
 */
public class Animal {
    /**
     * 类型，比如是 1-鸭子、2-鸡 3-就是Animal类型
     */
    private String type;
    /**
     * 名字
     */
    private String name;

    public Animal() {
    }

    public Animal(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
