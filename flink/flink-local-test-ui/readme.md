# 本地运行 产生 web ui

1. 引入依赖
```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-runtime-web</artifactId>
    <version>1.17.1</version>
</dependency>
```

2. 代码中开启web ui

```java
// 获取执行环境
Configuration configuration = new Configuration();
// 设置本地 web ui 的端口
configuration.setInteger("rest.port", 9999);
StreamExecutionEnvironment environment = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(configuration);
```