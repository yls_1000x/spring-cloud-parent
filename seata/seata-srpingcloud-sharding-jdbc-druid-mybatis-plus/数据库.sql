create database seata_account;
use seata_account;
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB COMMENT ='AT transaction mode undo table';

create database seata_order;
use seata_order;
create table account_order(
    id int unsigned auto_increment primary key comment '主键',
    account_id int unsigned comment '账户id',
    amount bigint comment '订单金额,单位分',
    order_time datetime comment '订单时间'
) engine=InnoDB comment '账户订单表';
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB COMMENT ='AT transaction mode undo table';

create table account_orders_0(
    id int unsigned auto_increment primary key comment '主键',
    account_id int unsigned comment '账户id',
    amount bigint comment '订单金额,单位分',
    order_time timestamp comment '订单时间'
) engine=InnoDB comment '账户订单表';

create table account_orders_1(
    id int unsigned auto_increment primary key comment '主键',
    account_id int unsigned comment '账户id',
    amount bigint comment '订单金额,单位分',
    order_time timestamp comment '订单时间'
) engine=InnoDB comment '账户订单表';

-- 账户库
select * from seata_account.account;
select * from seata_account.undo_log;

-- 订单库
select * from seata_order.account_order;
select * from seata_order.undo_log;
truncate table seata_order.account_order;
select * from seata_order.account_orders_0;
select * from seata_order.account_orders_1;

-- seata 库
use seata;
select * from seata.global_table;
select * from seata.branch_table;
select * from seata.lock_table;