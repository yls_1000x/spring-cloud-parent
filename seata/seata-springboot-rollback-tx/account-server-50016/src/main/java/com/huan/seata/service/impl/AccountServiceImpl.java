package com.huan.seata.service.impl;

import com.huan.seata.mapper.AccountMapper;
import com.huan.seata.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author huan.fu 2021/9/16 - 下午2:01
 */
@Service
@Slf4j
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {
    
    private final AccountMapper accountMapper;
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void debit(Integer id, Long amount) {
        log.info("准备扣除用户id:[{}][{}]分钱.", id, amount);
        accountMapper.debit(id, amount);
    }
}
