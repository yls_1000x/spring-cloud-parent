package com.huan.seata.service;

/**
 * @author huan.fu 2021/9/16 - 下午2:34
 */
public interface OrderService {
    
    void createOrder(Integer accountId, Long amount);
}
