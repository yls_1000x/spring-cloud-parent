package com.huan.seata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * @author huan.fu 2021/9/27 - 下午2:33
 */
@Getter
@Setter
@ToString
@TableName("order")
public class OrderDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer accountId;
    private Long amount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp orderTime;
}
