package com.huan.hadoop.mr;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * map操作
 * 此处需要将 PhoneFlow对象作为输出的Key
 *
 * @author huan.fu
 * @date 2023/7/12 - 19:16
 */
public class PhoneFlowSortMapper extends Mapper<LongWritable, Text, PhoneFlow, Text> {

    private final PhoneFlow outKey = new PhoneFlow();
    private final Text outValue = new Text();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, PhoneFlow, Text>.Context context) throws IOException, InterruptedException {
        // 获取文件中的一行数据 1	13736230513	192.196.100.1	www.baidu.com	2481	24681	200
        String row = value.toString();
        // 进行切割 [1,13736230513,192.196.100.1,www.baidu.com,2481,24681,200]
        String[] cells = row.split("\\s+");
        // 获取手机号
        String phone = cells[1];
        outValue.set(phone);

        // 获取上行流量
        long upFlow = Long.parseLong(cells[4]);
        // 获取下行流量
        long downFlow = Long.parseLong(cells[5]);
        outKey.setUpFlow(upFlow);
        outKey.setDownFlow(downFlow);

        // 输出
        context.write(outKey, outValue);
    }
}
