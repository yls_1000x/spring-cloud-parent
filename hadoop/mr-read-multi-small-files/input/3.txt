Example: WordCount v1.0
Before we jump into the details, lets walk through an example MapReduce application to get a flavour for how they work.
WordCount is a simple application that counts the number of occurrences of each word in a given input set.
This works with a local-standalone, pseudo-distributed or fully-distributed Hadoop installation (Single Node Setup).