# 1、需求

实现自定义Hadoop中的序列化操作。
根据给入的数据`phone_flow.txt`, 每行的数据格式`phone_flow_data_format.txt`希望输出如下格式

```text
13560436666 		1116		      954 			2070
手机号码		        上行流量           下行流量		总流量
```

即统计每个手机号的 上行流量、下行流量和总流量

# 2、实现步骤

（1）必须实现`Writable`接口  
（2）反序列化时，需要反射调用空参构造函数，所以必须有空参构造

```java
public FlowBean(){
        super();
        }
```

（3）重写序列化方法

```java
@Override
public void write(DataOutput out)throws IOException{
        out.writeLong(upFlow);
        out.writeLong(downFlow);
        out.writeLong(sumFlow);
        }
```

（4）重写反序列化方法

```java
@Override
public void readFields(DataInput in)throws IOException{
        upFlow=in.readLong();
        downFlow=in.readLong();
        sumFlow=in.readLong();
        }
```

（5）注意反序列化的顺序和序列化的顺序完全一致  
（6）要想把结果显示在文件中，需要重写toString()，可用"\t"分开，方便后续用。  
（7）如果需要将自定义的bean放在key中传输，则还需要实现Comparable接口，因为MapReduce框中的Shuffle过程要求对key必须能排序。必须使用 WritableComparable 接口，否则报错

```java
@Override
public int compareTo(FlowBean o){
        // 倒序排列，从大到小
        return this.sumFlow>o.getSumFlow()?-1:1;
        }
```