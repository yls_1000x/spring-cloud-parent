1. 基于 Map Reduce的 word count案例,并且 map 端的输出和reduce端的输出采用 bzip2压缩
   1. 压缩主要是修改Driver类的代码
      1. 开启map端的压缩
```java
// 开启map端输出压缩
configuration.setBoolean("mapreduce.map.output.compress", true);
// 设置map端输出压缩方式
configuration.setClass("mapreduce.map.output.compress.codec", BZip2Codec.class, CompressionCodec.class);

```

      2. 开启reduce端的压缩
```java
// 设置reduce端输出压缩开启
FileOutputFormat.setCompressOutput(job, true);
// 设置压缩的方式
FileOutputFormat.setOutputCompressorClass(job, BZip2Codec.class);
```
