# 1、需求

从数据库中读取数据，并写入到本地文件中

# 2、实现步骤

1. 编写一个实体类 `Student` 实现 `DBWritable` 接口
2. 编写 Mapper 类， 输出的key不用， value 为读出的行数据
3. 编写驱动类

```java
// 配置当前作业需要的JDBC密码
DBConfiguration.configureDB(
        configuration,
        "com.mysql.cj.jdbc.Driver",
        "jdbc:mysql://localhost:3306/temp_work",
        "root",
        "root@1993"
);

// 此处不需要reduce task 设置为 0
job.setNumReduceTasks(0);
// 设置数据输入组件
job.setInputFormatClass(DBInputFormat.class);
DBInputFormat.setInput(job, Student.class,
"select * from student",
"select count(*) from student");
// 设置数据输出组件
FileOutputFormat.setOutputPath(job, new Path("/tmp/mr-read-from-db"));

```

# 3、查看输出的文件名

```shell
➜  mr-read-from-db pwd
/tmp/mr-read-from-db
➜  mr-read-from-db ll
total 8
-rw-r--r--  1 huan  wheel     0B  7 16 10:43 _SUCCESS
-rw-r--r--  1 huan  wheel    18B  7 16 10:43 part-m-00000
➜  mr-read-from-db cat part-m-00000
1	张三
2	李四
➜  mr-read-from-db
```

可以看到输出的文件名为`part-m-00000`，中间的`m`就表示是map阶段输出的。
