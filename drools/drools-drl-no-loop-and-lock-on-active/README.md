# 实现功能

1、死循环的解决  (no-loop true || lock-on-active true)  
2、同一个组中的规则绝对执行一次(lock-on-active true)

# 结论

`no-loop`：针对的当前规则的修改，即当前规则的`RHS`部分调用了`insert/modify/delete`等方法，导致重新匹配上当前规则，阻止重新执行。如果是
别的规则导致当前规则触发，则无法阻止。  
`lock-on-active`：保证规则只执行一次。不管是别的规则触发还是当前规则触发，都可以保证。

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124843909](https://blog.csdn.net/fu_huo_1993/article/details/124843909)

# 参考链接

1、[https://stackoverflow.com/questions/17042437/what-is-the-difference-between-no-loop-and-lock-on-active-in-drools](https://stackoverflow.com/questions/17042437/what-is-the-difference-between-no-loop-and-lock-on-active-in-drools)  
2、[https://ilesteban.wordpress.com/2012/11/16/about-drools-and-infinite-execution-loops/](https://ilesteban.wordpress.com/2012/11/16/about-drools-and-infinite-execution-loops/)