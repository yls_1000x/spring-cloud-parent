package com.huan.drools.controller;

import com.huan.drools.fact.Person;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huan.fu
 * @date 2022/5/25 - 18:02
 */
@RestController
public class PersonController {

    private final KieContainer kieContainer;

    public PersonController(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    /**
     * 调用drools rule判断用户是否可以玩游戏
     */
    @GetMapping("canPlayGame")
    public Person canPlayGame(Person person) {
        KieSession kieSession = kieContainer.newKieSession("ksession");
        try {
            kieSession.insert(person);
            kieSession.fireAllRules();
        } finally {
            kieSession.dispose();
        }
        return person;
    }
}
