package com.huan.drools;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author huan.fu
 * @date 2022/5/24 - 09:35
 */
@Getter
@Setter
@AllArgsConstructor
public class Order {
    private Long orderId;
    private Long price;
}
