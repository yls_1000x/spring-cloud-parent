package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

/**
 * 告警对象
 *
 * @author huan.fu
 * @date 2022/5/24 - 12:36
 */
@Getter
@Setter
public class Alarm {
    private Fire fire;

    public Alarm(Fire fire) {
        this.fire = fire;
    }
}
