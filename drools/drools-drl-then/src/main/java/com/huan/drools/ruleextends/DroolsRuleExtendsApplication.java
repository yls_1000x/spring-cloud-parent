package com.huan.drools.ruleextends;

import com.huan.drools.Car;
import com.huan.drools.Customer;
import com.huan.drools.Fire;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * rule extends 测试rule规则的继承
 */
public class DroolsRuleExtendsApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        Car car = new Car();
        Customer customer = new Customer();
        customer.setAge(65);
        kieSession.insert(customer);
        kieSession.insert(car);
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("rule_extends_"));

        kieSession.dispose();
    }
}
