# 实现功能

1、更新工作内存中的fact对象 inset/insertLogical/modify/update/delete 等方法的使用  
2、drools变量在drl文件中的使用。  
3、规则的继承。  
4、命名结果，可以实现规则的继承功能. do[..] then then[..]  
5、then 中可以实现 if elseif else 的功能

if 后面 `do`和 `break`的区别。  
`do`：执行完之后，还会继续判断后面的执行条件。  （`即还会执行后面的Car判断，根据是否有Car获取不同的结果`）  
`break`：执行完之后，不会在判断后面的执行条件。（`即忽略了后面的Car判断，rule执行完了`）

# 博客地址

[https://blog.csdn.net/fu_huo_1993/article/details/124944449](https://blog.csdn.net/fu_huo_1993/article/details/124944449)

# 参考链接

[https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-rules-THEN-con_drl-rules](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-rules-THEN-con_drl-rules)
