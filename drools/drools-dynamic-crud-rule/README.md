# 实现功能

1. 规则的内容需要从数据库中动态加载出来，例子中是保存在内存中。  
2. 需要创建多个`KieBase`，实现规则的隔离。  
3. 可以动态的更新或添加规则。  
4. 可以删除规则。  

# 核心知识点

1、`KieContainer`只可存在一个，不可多次构建，然后进行覆盖操作。覆盖操作，那么之前创建的KieContainer需要销毁掉，那么它的Session也需要销毁，
这样可能存在问题。  
2、`KieFileSystem`这是一个基于内存的文件系统，也是每个`KieContainer`只能有一个。如果每次都是新的，那么当添加一个新的规则
可能导致之前的规则失效。  
3、`kmodule.xml`这个需要通过编程动态构建出来。  
4、调用` ((KieContainerImpl) kieContainer).updateToKieModule((InternalKieModule) kieBuilder.getKieModule());`方法完成规则的
动态刷新。  
5、删除规则通过`KieBase`来操作即可。  
6、By default, a KieBase at runtime can be updated dynamically and incrementally with all the running KieSessions that are created from that KieBase. However, the running KieSessions need additional synchronization points to make the dynamic update possible and consistent. Therefore, a new KieBaseMutabilityOption is introduced to optionally disable the possibility of performing an incremental update on a given KieBase.
The new `KieBaseMutabilityOption` consists of two values including `ALLOWED` and `DISABLED`. The default value of KieBaseMutabilityOption is ALLOWED. However, when an incrementally update of the KieBase is not required, then set the value of KieBaseMutabilityOption as DISABLED to avoid the performance costs caused by the previously mentioned synchronization points. It is possible to set this option either in the kmodule.xml file for a specific KieBase or using the following system property for all KIE bases.
Example kmodule.xml file
```
<kbase name="KBase1" mutability="disabled">
```
System property for KieBaseMutabilityOption
```
drools.kieBaseMutability=disabled
```

# 案例
## 测试规则的动态添加
### 1、添加规则
```shell
curl --location --request POST 'http://localhost:8080/drools/rule/add' \
--header 'User-Agent: apifox/1.0.0 (https://www.apifox.cn)' \
--header 'Content-Type: application/json' \
--data-raw '{
    "ruleId": 1,
    "kieBaseName": "kieBase01",
    "kiePackageName": "rules.rule01",
    "ruleContent": "package rules.rule01 \n global java.lang.StringBuilder resultInfo \n rule \"rule-01\" when $i: Integer() then resultInfo.append(drools.getRule().getPackageName()).append(\".\").append(drools.getRule().getName()).append(\"执行了，前端传递的参数:\").append($i); end"
}'
```
```java
package rules.rule01

global java.lang.StringBuilder resultInfo

rule "rule-01"
    when
        $i: Integer()
    then
        resultInfo.append(drools.getRule().getPackageName()).append(".").append(drools.getRule().getName()).append("执行了，前端传递的参数:").append($i);
end
```
### 2、运行
```shell
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase01\&param\=1
rules.rule01.rule-01执行了，前端传递的参数:1%
➜  ~
```
可以看到我们动态加载的规则执行了。

## 修改规则
**需求：** 在 `6、测试规则的动态添加`的基础上，修改规则。

之前的规则
```java
package rules.rule01

global java.lang.StringBuilder resultInfo

rule "rule-01"
    when
        $i: Integer()
    then
        resultInfo.append(drools.getRule().getPackageName()).append(".").append(drools.getRule().getName()).append("执行了，前端传递的参数:").append($i);
end
```
修改后的规则
```java
package rules.rule01

global java.lang.StringBuilder resultInfo

rule "rule-01"
    when
        $i: Integer(intValue() > 3) // 注意此处修改了
    then
        resultInfo.append(drools.getRule().getPackageName()).append(".").append(drools.getRule().getName()).append("执行了，前端传递的参数:").append($i);
end
```
可以看到修改的地方为`$i: Integer(intValue() > 3)`，即增加了一个条件判断。

### 1、修改规则
```shell
➜  ~ curl --location --request POST 'http://localhost:8080/drools/rule/update' \
--header 'User-Agent: apifox/1.0.0 (https://www.apifox.cn)' \
--header 'Content-Type: application/json' \
--data-raw '{
    "ruleId": 1,
    "kieBaseName": "kieBase01",
    "kiePackageName": "rules.rule01",
    "ruleContent": "package rules.rule01 \n global java.lang.StringBuilder resultInfo \n rule \"rule-01\" when $i: Integer(intValue() > 3) then resultInfo.append(drools.getRule().getPackageName()).append(\".\").append(drools.getRule().getName()).append(\"执行了，前端传递的参数:\").append($i); end"
}'
```
此处修改了规则内存中`Integer`的值必须`>3`时才执行。
### 2、运行
```shell
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase01\&param\=1
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase01\&param\=6
rules.rule01.rule-01执行了，前端传递的参数:6%
➜  ~
```
从上面可以看到，当我们传递的`param=1`时，没有结果数据，当`param=6`时有结果输出。

## 删除
**需求：**  将上一步创建的规则删除
### 1、删除规则
```shell
➜  ~ curl --location --request POST 'http://localhost:8080/drools/rule/deleteRule?ruleId=1&ruleName=rule-01'

删除成功%
➜  ~
```
### 2、运行结果
```shell
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase01\&param\=6
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase01\&param\=1
➜  ~
```
可以看到删除成功了。

## 模拟2个kbase
### 1、添加规则并执行
```shell
➜  ~ curl --location --request POST 'http://localhost:8080/drools/rule/add' \
--header 'Content-Type: application/json' \
--data-raw '{
    "ruleId": 1,
    "kieBaseName": "kieBase01",
    "kiePackageName": "rules.rule01",
    "ruleContent": "package rules.rule01 \n global java.lang.StringBuilder resultInfo \n rule \"rule-01\" when $i: Integer() then resultInfo.append(drools.getRule().getPackageName()).append(\".\").append(drools.getRule().getName()).append(\"执行了，前端传递的参数:\").append($i); end"
}'
添加成功%
➜  ~ curl --location --request POST 'http://localhost:8080/drools/rule/add' \
--header 'Content-Type: application/json' \
--data-raw '{
    "ruleId": 2,
    "kieBaseName": "kieBase02",
    "kiePackageName": "rules.rule02",
    "ruleContent": "package rules.rule02 \n global java.lang.StringBuilder resultInfo \n rule \"rule-01\" when $i: Integer() then resultInfo.append(drools.getRule().getPackageName()).append(\".\").append(drools.getRule().getName()).append(\"执行了，前端传递的参数:\").append($i); end"
}'
添加成功%
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase01\&param\=1
rules.rule01.rule-01执行了，前端传递的参数:1%
➜  ~ curl http://localhost:8080/drools/rule/fireRule\?kieBaseName\=kieBase02\&param\=1
rules.rule02.rule-01执行了，前端传递的参数:1%
➜  ~
```
### 2、执行
![在这里插入图片描述](https://img-blog.csdnimg.cn/b33d740cd986465da1475c69161de8ab.jpeg#pic_center)

# 博客地址

[https://blog.csdn.net/fu_huo_1993/article/details/124998602](https://blog.csdn.net/fu_huo_1993/article/details/124998602)

# 参考链接

1、[https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#_definingakiemoduleprogrammatically](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#_definingakiemoduleprogrammatically)
