package com.huan.drools;

import lombok.extern.slf4j.Slf4j;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * drools 决策表的使用
 */
@Slf4j
public class DroolsDecisionTableApplication {
    public static void main(String[] args) throws IOException {
        decisionTable2Drl();
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.newKieClasspathContainer();
        // 张三虽然只得20分，但是根据规则判断，结果应该是  优
        invokedDecisionTable(kieContainer, new Student("张三", 20));
        // 李四虽然只得20分，但是根据规则判断，结果应该是  一般
        invokedDecisionTable(kieContainer, new Student("李四", 20));
        // 李四得75分，但是根据规则判断，结果应该是  良好
        invokedDecisionTable(kieContainer, new Student("李四", 75));
        // 王五得59分，但是根据规则判断，结果应该是  不及格
        invokedDecisionTable(kieContainer, new Student("王五", 59));
        // 赵六得20分，但是根据规则判断，结果应该是  一般
        invokedDecisionTable(kieContainer, new Student("赵六", 65));
        // 钱七得20分，但是根据规则判断，结果应该是  良好
        invokedDecisionTable(kieContainer, new Student("钱七", 75));
        // 李八得20分，但是根据规则判断，结果应该是  优
        invokedDecisionTable(kieContainer, new Student("李八", 95));
    }

    public static void invokedDecisionTable(KieContainer kieContainer, Student student) {
        System.out.println("\r");
        KieSession kieSession = kieContainer.newKieSession("ksession");
        StringBuilder result = new StringBuilder();
        kieSession.setGlobal("resultsInfo", result);
        kieSession.insert(student);
        kieSession.fireAllRules();
        kieSession.dispose();
        System.out.println("规则执行结果：" + result);
    }

    /**
     * 决策表转换成 drl 文件
     */
    public static void decisionTable2Drl() throws IOException {
        Resource resource = ResourceFactory.newClassPathResource("rules/decision/tables/student-score.xlsx", "UTF-8");
        InputStream inputStream = resource.getInputStream();
        SpreadsheetCompiler compiler = new SpreadsheetCompiler();
        String drl = compiler.compile(inputStream, InputType.XLS);
        log.info("决策表转换的drl内容为:\r{}", drl);
        // 验证一下 drl 文件是否有问题
        KieHelper kieHelper = new KieHelper();
        Results results = kieHelper.addContent(drl, ResourceType.DRL).verify();
        List<Message> messages = results.getMessages(Message.Level.ERROR);
        if (null != messages && !messages.isEmpty()) {
            for (Message message : messages) {
                log.error(message.getText());
            }
        }
    }
}
