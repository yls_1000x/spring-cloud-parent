# 功能

1、使用session 池化技术，提高性能。  

# 使用

```java
public class StatefulSessionApplication {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();
        // 初始化 10 个session，如果在运行的过程中，需要超过10个，则会自己动态的扩展
        KieContainerSessionsPool sessionsPool = kieClasspathContainer.newKieSessionsPool(10);

        KieSession kieSession = sessionsPool.newKieSession("stateful-session");

        ......

        // 有状态的Session最后一定要调用此方法，防止内存泄漏，此时session不会销毁，会再次加入到池中
        kieSession.dispose();

        // 关闭session pool 防止内存泄漏 sessionsPool.shutdown(); 或者  kieClasspathContainer.dispose(); 都可以
        sessionsPool.shutdown();
    }
}

```

``` 
KieContainerSessionsPool sessionsPool = kieClasspathContainer.newKieSessionsPool(10);
```
上方这句表达的意思是，初始化10个session，而不是只有10个session，当需要超过10个session的时候，可以自动扩展。

# 池的销毁

1、调用 sessionsPool.shutdown(); 方法  
2、调用 kieClasspathContainer.dispose(); 方法  

以上2个方法都可以。