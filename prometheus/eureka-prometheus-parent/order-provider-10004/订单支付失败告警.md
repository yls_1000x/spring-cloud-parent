# 在1分钟之内支付订单支付失败产生告警数据

## 一、支付订单失败产生告警

### 1、需求

假设我们有一个订单支付场景，当支付失败时，产生告警信息。

### 2、告警规则的编写

`订单在1分钟内产生了告警信息。`

**PromQL语句：**

```
payment_fail_order < time() and payment_fail_order > time() - 60
```



```yaml
groups:
  - name: Order-Group-001 # 组的名字，在这个文件中必须要唯一
    rules:
      - alert: OrderPaymentFail # 告警的名字，在组中需要唯一
        expr: "payment_fail_order < time() and payment_fail_order > time() - 60" # 表达式, 执行结果为true: 表示需要告警
        for: 0s # 超过多少时间才认为需要告警(即up==0需要持续的时间)
        labels:
          severity: warning # 定义标签
        annotations:
          summary: "订单 {{ $labels.orderId }} 支付失败了"
          description: "{{ $labels.instance }} of job {{ $labels.job }} 的订单 {{$labels.orderId}} 在最近1分钟内 产生了告警,告警级别 {{$labels.level}}."
```

### 3、alertmanager.yaml修改

```yaml
global:
  resolve_timeout: 5m
  smtp_smarthost: 'smtp.qq.com:465'
  smtp_from: '1451578387@qq.com'
  smtp_auth_username: '1451578387@qq.com'
  smtp_auth_identity: 'gmrkcxqhsozgiffd'
  smtp_auth_password: 'gmrkcxqhsozgiffd'
  smtp_require_tls: false
route:
  group_by: ['job']
  group_wait: 10s
  group_interval: 10s
  repeat_interval: 120s
  receiver: 'default-receiver'
  routes:
  - match_re:
      alertname: 'OrderPaymentFail'
    receiver: 'receiver-00'
    group_wait: 0s
    group_interval: 1s
    repeat_interval: 120s

receivers:
  - name: 'default-receiver'
    email_configs:
      - to: '1451578387@qq.com'
        send_resolved: true
  - name: 'receiver-00'
    email_configs:
      - to: '1451578387@qq.com'
        send_resolved: false   # 不发送告警修复通知
```

### 4、模拟产生告警数据

```java
/**
     * 产生告警
     */
    @GetMapping("orderPaymentFail")
    public String orderPaymentFail(@RequestParam("orderId") String orderId,
                                   @RequestParam("level") String level) {
        final long alertTime = System.currentTimeMillis() / 1000;
        // 对于同一个时间序列，指标名称+标签+标签值 要一致，如果不一致则可能产生一个新的。
        // 比如：如果想记录发生的时间、分布式追踪id 等，那么则可能每次都会生成一个新的时间序列。
        Gauge.builder("payment_fail_order", () -> alertTime)
                .tag("module", "order")
                .tag("orderId", orderId)
                // 不给时间，不然相同的订单每次产生的告警信息都不一样
                // .tag("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))
                .tag("level", level)
                .description("描述")
                .register(meterRegistry);
        return "产生一个告警";
    }
```

![时间序列数据](https://gitee.com/huan1993/images/raw/master/blog/image-20210324165124912.png)

### 5、产生告警结果

![image-20210324163345303](https://gitee.com/huan1993/images/raw/master/blog/image-20210324163345303.png)

### 5、代码路径

[https://gitee.com/huan1993/spring-cloud-parent/tree/master/prometheus/eureka-prometheus-parent/order-provider-10004/src/main/java/com/huan/prometheus/模拟业务支付失败告警