# 需求

1、日志以非json格式输出，优化异常堆栈。  
2、日志以json格式输出，优化异常队长。

# 实现

## 1、日志以非json格式输出，优化异常堆栈。

```xml
<conversionRule conversionWord="stack"
                    converterClass="net.logstash.logback.stacktrace.ShortenedThrowableConverter"/>

    <!--
        stack{5,1024,10,rootFirst,regex1,regex2,evaluatorName}
        参数解释： 建议查看源码 net.logstash.logback.stacktrace.ShortenedThrowableConverter#parseOptions() 方法
            第1参数： maxDepthPerThrowable：值可以是 full或者short或者int值，表示每个异常最多打印多少个 stackTraceElements 元素
            第2参数： shortenedClassNameLength：值可以是 full或者short或者int值，将尝试将类名长度缩短到小于此值
                     com.huan.springboot.service.ExceptionService 可能会变成 c.h.s.s.ExceptionService
            第3参数： maxLength：值可以是 full或者short或者int值，指的输出到日志中整个堆栈最大能存在多少个字符。
            后面这些参数没有固定顺序
            第4参数： rootFirst： 可选参数，如果使用该参数，值就是 rootFirst ,表示应首先打印堆栈的根本原因
            第5参数： inlineHash： 可选参数，如果使用该参数，值就是 inlineHash, 指示应该计算和内联十六进制错误哈希
            如果参数都不是上方的类型，那么可能是 evaluator 或者 exclude 类型，这2个都是判断这个 stackTraceElement 是否应该被打印，这2个参数没有顺序关系
                evaluator： 值的是需要实现 EventEvaluator<ILoggingEvent> 的类
                exclude： 指的是需要排除的正则表达式， 如果存在.需要转义成 \.
    -->

    <!--打印到控制台-->
    <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">

<!--        <evaluator class="com.huan.springboot.evaluator.CustomEventEvaluator"/>-->

        <encoder>
            <!--            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [${PID:- }] [%thread] %-5level %logger{50}#%method:%L -%msg%n%stack{5,1024,10,rootFirst,regex1,regex2,evaluatorName}</pattern>-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread]
                %msg%n%stack{2,300,2048,rootFirst,inlineHash,"aa"}
            </pattern>
            <charset>UTF-8</charset>
        </encoder>
    </appender>
```

## 2、日志以json格式输出，优化异常队长。

```xml
<appender name="file" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>logs/springboot-spring-%d{yyyy-MM-dd}-.%i.log</fileNamePattern>
            <maxHistory>7</maxHistory>
            <maxFileSize>1MB</maxFileSize>
            <totalSizeCap>2GB</totalSizeCap>
        </rollingPolicy>
        <encoder class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <!-- 格式化json输出 -->
            <jsonGeneratorDecorator class="net.logstash.logback.decorate.PrettyPrintingJsonGeneratorDecorator"/>

            <providers>
                <!--
                    1、下方可编写的 mdc、timestamp 标签等，具体有那些呢？参考 https://github.com/logfellow/logstash-logback-encoder#provider_pattern
                -->
                <mdc/>
                <!-- provides the timestamp net.logstash.logback.LogstashFormatter-->
                <timestamp>
                    <pattern>yyyy-MM-dd'T'HH:mm:ss.SSS</pattern>
                    <timeZone>UTC+8</timeZone>
                    <fieldName>logTime</fieldName>
                </timestamp>

                <!-- provides the version -->
                <!--                <version>-->
                <!--                    <fieldName>version</fieldName>-->
                <!--                    <writeAsInteger>true</writeAsInteger>-->
                <!--                </version>-->

                <callerData>
                    <methodFieldName>method</methodFieldName>
                    <lineFieldName>line</lineFieldName>
                    <classFieldName>[ignore]</classFieldName>
                    <fileFieldName>[ignore]</fileFieldName>
                </callerData>

                <stackHash>
                    <fieldName>stackHash</fieldName>
                </stackHash>

                <stackTrace>
                    <fieldName>stack_trace</fieldName>
                    <throwableConverter class="net.logstash.logback.stacktrace.ShortenedThrowableConverter">
                        <maxDepthPerThrowable>50</maxDepthPerThrowable>
                        <maxLength>2048</maxLength>
                        <shortenedClassNameLength>20</shortenedClassNameLength>
                        <!-- 排除不需要的堆栈信息 -->
                        <exclude>^net\.sf\.cglib\.proxy\.MethodProxy\.invoke</exclude>
                        <exclude>^org\.apache\.*</exclude>
                        <exclude>^sun\.reflect\.*</exclude>
                        <exclude>^sun\.reflect\.*</exclude>
                        <exclude>^java\.lang\.reflect\.*</exclude>
                        <evaluator class="com.huan.springboot.evaluator.CustomEventEvaluator"/>
                        <rootCauseFirst>true</rootCauseFirst>
                    </throwableConverter>
                </stackTrace>

                <pattern>
                    <pattern>
                        {
                        "logger": "%logger",
                        "pid": "${PID:-}",
                        "level": "%level",
                        "thread": "%thread",
                        "message": "%message",
                        "traceId": "%mdc{traceId}",
                        "num": "%mdc{num}",
                        "appname":"springboot-logback-optimize-exception"
                        }
                    </pattern>
                </pattern>
            </providers>
        </encoder>

    </appender>
```

# 参考连接

1、[https://github.com/logfellow/logstash-logback-encoder](https://github.com/logfellow/logstash-logback-encoder)  
2、[自定义字段可能存在的名字](net.logstash.logback.fieldnames.LogstashFieldNames)    
3、[需要排除的异常堆栈](https://github.com/logfellow/logstash-logback-encoder/blob/main/stack-hash.md)