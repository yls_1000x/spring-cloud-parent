package com.huan.ohc;

import org.caffinitas.ohc.CacheSerializer;
import org.caffinitas.ohc.Eviction;
import org.caffinitas.ohc.OHCache;
import org.caffinitas.ohc.OHCacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * 初始化堆外内存
 *
 * @author huan.fu
 * @date 2023/5/16 - 20:32
 */
@Configuration
public class InitStackOutsideMemoryConfig {

    @Bean
    public OHCache<String, String> ohCache() {
        return OHCacheBuilder.<String, String>newBuilder()
                // key 的序列化
                .keySerializer(new StringCacheSerializer())
                // value 的序列化
                .valueSerializer(new StringCacheSerializer())
                // 默认会使用64M堆外内存，此处使用2G
                .capacity(2 * 1024 * 1024 * 1024L)
                // 使用lru的算法
                .eviction(Eviction.LRU)
                // 缓存过期时间 5s
                .defaultTTLmillis(5 * 1000L)
                // true: 标识缓存会过期
                .timeouts(true)
                .build();
    }
}
