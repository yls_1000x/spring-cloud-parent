package com.huan.study.context.parent;

import com.huan.study.context.CommonApi;
import com.huan.study.context.product.ProductApiConfiguration;
import com.huan.study.context.user.UserApiConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author huan.fu 2021/6/8 - 下午2:07
 */
@SpringBootApplication
public class ContextApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext parentContext = SpringApplication.run(ContextApplication.class);

        AnnotationConfigApplicationContext userApiContext = new AnnotationConfigApplicationContext();
        userApiContext.register(UserApiConfiguration.class);
        userApiContext.register(DefaultCommonApiConfiguration.class);
        userApiContext.setParent(parentContext);
        userApiContext.setClassLoader(parentContext.getClassLoader());
        userApiContext.refresh();

        AnnotationConfigApplicationContext productApiContext = new AnnotationConfigApplicationContext();
        productApiContext.register(ProductApiConfiguration.class);
        productApiContext.register(DefaultCommonApiConfiguration.class);
        userApiContext.setParent(parentContext);
        productApiContext.setClassLoader(parentContext.getClassLoader());
        productApiContext.refresh();

        CommonApi defaultCommonApi = parentContext.getBean(CommonApi.class);
        System.out.println("parent api context: " + defaultCommonApi.apiName());

        CommonApi userApi = userApiContext.getBean(CommonApi.class);
        System.out.println("user api context: " + userApi.apiName());

        CommonApi productApi = productApiContext.getBean(CommonApi.class);
        System.out.println("product api context: " + productApi.apiName());
    }
}
