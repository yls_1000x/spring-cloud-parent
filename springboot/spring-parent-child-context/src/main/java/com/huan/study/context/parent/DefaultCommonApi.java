package com.huan.study.context.parent;

import com.huan.study.context.CommonApi;

/**
 * @author huan.fu 2021/6/8 - 下午2:31
 */
public class DefaultCommonApi implements CommonApi {
    @Override
    public String apiName() {
        return "default common api";
    }
}
