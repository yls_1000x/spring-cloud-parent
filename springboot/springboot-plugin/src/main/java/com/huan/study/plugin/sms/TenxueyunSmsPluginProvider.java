package com.huan.study.plugin.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 腾讯云渠道发送短信
 * <pre>@Order</pre> 注解用于指定插件的顺序
 *
 * @author huan.fu 2021/10/22 - 下午3:45
 */
@Component
@Order(1)
public class TenxueyunSmsPluginProvider implements SmsPlugin {
    
    private static final Logger log = LoggerFactory.getLogger(TenxueyunSmsPluginProvider.class);
    
    @Override
    public boolean supports(SmsType smsType) {
        return smsType == SmsType.TENXUNYUN;
    }
    
    @Override
    public void sendSms(String phone, String content) {
        log.info("通过腾讯云渠道 给phone:[{}]发送短信:[{}]成功", phone, content);
    }
}
