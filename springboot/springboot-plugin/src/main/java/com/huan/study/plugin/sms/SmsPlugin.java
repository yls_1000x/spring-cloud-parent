package com.huan.study.plugin.sms;

import org.springframework.plugin.core.Plugin;

/**
 * 短信插件
 *
 * @author huan.fu 2021/10/22 - 下午3:44
 */
public interface SmsPlugin extends Plugin<SmsType> {
    /**
     * 发送短信
     *
     * @param phone   手机号
     * @param content 短信内容
     */
    void sendSms(String phone, String content);
}
