package com.huan.xss;

import com.huan.xss.config.XssProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(XssProperties.class)
public class SpringbootXssApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootXssApplication.class, args);
	}

}
