package com.huan.xss.core;

/**
 * xss 清理器
 *
 * @author huan.fu
 */
public interface XssCleaner {


	/**
	 * 清理 html
	 * @param html html
	 * @return 清理后的数据
	 */
	String clean(String html);
}