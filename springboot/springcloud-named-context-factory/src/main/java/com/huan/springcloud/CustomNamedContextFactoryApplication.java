package com.huan.springcloud;


import com.huan.springcloud.common.CommonApi;
import com.huan.springcloud.config.ChildClientNamedContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

/**
 * 测试在 Spring Cloud中自定义父子上下文
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:16
 */
@SpringBootApplication
public class CustomNamedContextFactoryApplication implements ApplicationRunner {

    @Resource
    private ChildClientNamedContextFactory childClientNamedContextFactory;
    @Resource
    private CommonApi commonApi;

    public static void main(String[] args) {
        SpringApplication.run(CustomNamedContextFactoryApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        commonApi.showName();
        childClientNamedContextFactory.getInstance("product", CommonApi.class).showName();
        childClientNamedContextFactory.getInstance("user", CommonApi.class).showName();
    }
}
