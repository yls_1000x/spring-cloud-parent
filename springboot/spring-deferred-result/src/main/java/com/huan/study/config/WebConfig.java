package com.huan.study.config;

import com.huan.study.interceptor.CustomAsyncHandlerInterceptor;
import com.huan.study.interceptor.CustomDeferredResultProcessingInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 如果加了 @EnableWebMvc 注解的话, Spring 很多默认的配置就没有了，需要自己进行配置
 *
 * @author huan.fu 2021/10/14 - 上午10:39
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        // 默认超时时间 60s
        configurer.setDefaultTimeout(60000);
        // 注册 deferred result 拦截器
        configurer.registerDeferredResultInterceptors(new CustomDeferredResultProcessingInterceptor());
    }
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CustomAsyncHandlerInterceptor()).addPathPatterns("/**");
    }
}
