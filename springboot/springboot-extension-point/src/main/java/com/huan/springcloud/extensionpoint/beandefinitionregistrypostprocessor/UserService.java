package com.huan.springcloud.extensionpoint.beandefinitionregistrypostprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author huan.fu
 * @date 2022/5/30 - 16:27
 */
@Component
public class UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);
    /**
     * 此处的 username 由CustomBeanDefinitionRegistryPostProcessor类来进行赋值操作
     */
    private String username;

    @PostConstruct
    public void init() {
        log.info("此处的username由CustomBeanDefinitionRegistryPostProcessor赋值,结果为：[{}]", username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
