package com.huan.study.beandefinition.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

/**
 * @author huan.fu 2021/4/14 - 上午11:13
 */
@Controller
public class OrderController {

    @Autowired
    private ProductService productService;

    @PostConstruct
    public void initShow() {
        System.out.println("获取到产品:" +
                productService.getProduct());
    }
}
