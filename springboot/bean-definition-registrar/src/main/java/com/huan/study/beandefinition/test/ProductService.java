package com.huan.study.beandefinition.test;

import com.huan.study.framewrok.CustomImport;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author huan.fu 2021/4/14 - 上午11:12
 */
@CustomImport(beanName = "productService")
public class ProductService {

    @Autowired
    private StockService stockService;

    public String getProduct() {
        return "苹果🍎剩余的库存为:" + stockService.stock();
    }
}
