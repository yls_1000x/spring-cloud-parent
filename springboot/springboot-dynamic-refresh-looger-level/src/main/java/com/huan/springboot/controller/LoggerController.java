package com.huan.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.boot.logging.LoggerConfiguration;
import org.springframework.boot.logging.LoggingSystem;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 日志控制器
 *
 * @author huan.fu
 * @date 2023/8/29 - 12:35
 */
@RestController
public class LoggerController {

    @Resource
    private LoggingSystem loggingSystem;

    private static final Logger log = LoggerFactory.getLogger(LoggerController.class);

    @GetMapping("printLog")
    public void printLog() {
        log.debug("这是 debug 级别的日志");
        log.info("这是 info 级别的日志");
    }

    /**
     * 获取 LoggerController 这个类的日志级别
     *
     * @return LoggerConfiguration
     */
    @GetMapping("getLoggerLevel")
    public LoggerConfiguration getLoggerLevel() {
        return loggingSystem.getLoggerConfiguration(LoggerController.class.getName());
    }

    /**
     * 设置日志的级别为debug
     */
    @GetMapping("setLoggerDebugLevel")
    public void setLoggerDebugLevel() {
        loggingSystem.setLogLevel(LoggerController.class.getName(), LogLevel.DEBUG);
    }

    /**
     * 设置日志的级别为info
     */
    @GetMapping("setLoggerInfoLevel")
    public void setLoggerInfoLevel() {
        loggingSystem.setLogLevel(LoggerController.class.getName(), LogLevel.INFO);
    }
}
