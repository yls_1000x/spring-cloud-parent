# 实现功能

1. 实现字段的自定义序列化操作

# 实现步骤

1. 定义一个类继承 com.fasterxml.jackson.databind.ser.std.StdSerializer 这个类。
2. 在需要自定义序列化的字段上增加 如下注解：
```java
public class Person{
    @JsonSerialize(using = ReportDefaultJsonSerializer.class, nullsUsing = ReportDefaultJsonSerializer.class)
    private int oldId;
}
```
3. 如果字段是引用类型，即可能存在 null 的值，则需要在 `@JsonSerialize` 的 `nullsUsing`指定自定义序列化的类，否则会是以null返回